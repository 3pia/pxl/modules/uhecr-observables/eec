// EEC Module for PXL, 17.11.09, Peter Schiffer
// This calculates the EEC of all RegionsOfInterest in a BasicContainer


#include "pxl/modules/Module.hh"
#include "pxl/modules/ModuleFactory.hh"
#include "pxl/astro.hh"
#include "pxl/core.hh"
#include <vector>
#include <iostream>
#include <sstream>
#include <TH2.h>
#include <TH1.h>
#include <TFile.h>
#include <TProfile.h>
#include <TGraph.h>

#define MAXDIST 0.999999999

pxl::Logger logger("EECModule");

using namespace pxl;
using namespace std;

class EECModule: public pxl::Module {
private:
	// we want to have one sink (input) and one source (output)
	pxl::Sink _Sink;
	pxl::Source _Source;
	string _Name;
	TFile* _Outfile;
	string _OutputFolder;
	long _Count;
	long _NBins;
	double _SizeOfRoi;

	void uhecrstohist(BasicContainer* sky, TH2D* uhecrhist) {
		std::vector<UHECR*> myuhecrs;
		sky->getObjectsOfType<UHECR> (myuhecrs);

		for (std::vector<pxl::UHECR*>::iterator iter = myuhecrs.begin(); iter
				!= myuhecrs.end(); ++iter) {

			pxl::UHECR* cr = dynamic_cast<UHECR*> (*iter);

			if (cr) {
				uhecrhist->Fill(cr->getGalacticLongitude(),
						cr->getGalacticLatitude());
			}
		}
	}

	void uhecrstograph(BasicContainer* sky, TGraph* uhecrgraph) {
		std::vector<UHECR*> myuhecrs;
		sky->getObjectsOfType<UHECR> (myuhecrs);

		for (std::vector<pxl::UHECR*>::iterator iter = myuhecrs.begin(); iter
				!= myuhecrs.end(); ++iter) {

			pxl::UHECR* cr = dynamic_cast<UHECR*> (*iter);

			if (cr) {
				uhecrgraph->SetPoint(uhecrgraph->GetN(),
						cr->getGalacticLongitude(), cr->getGalacticLatitude());
			}
		}
	}

	void jetstohist(BasicContainer* sky, TH2D* jethist) {
		std::vector<RegionOfInterest*> myjets;
		sky->getObjectsOfType<RegionOfInterest> (myjets);

		for (std::vector<pxl::RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {

			std::vector<Serializable*> particleinroi;
			// create a pointer to the iter
			pxl::RegionOfInterest* roi = *iter;

			roi->getSoftRelations().getSoftRelatives(particleinroi, *sky);

			//remove use only UHECRs
			std::vector<UHECR*> crinroi;
			for (std::vector<pxl::Serializable*>::const_iterator iter2 =
					particleinroi.begin(); iter2 != particleinroi.end(); ++iter2) {
				pxl::UHECR* cr = dynamic_cast<UHECR*> (*iter2);
				if (cr) {
					jethist->Fill(cr->getGalacticLongitude(),
							cr->getGalacticLatitude());
				}
			}
		}
	}

	void jetstograph(BasicContainer* sky, TGraph* jetgraph) {
		std::vector<RegionOfInterest*> myjets;
		sky->getObjectsOfType<RegionOfInterest> (myjets);

		for (std::vector<pxl::RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {

			std::vector<Serializable*> particleinroi;
			// create a pointer to the iter
			pxl::RegionOfInterest* roi = *iter;
			roi->getSoftRelations().getSoftRelatives(particleinroi, *sky);

			//remove use only UHECRs
			std::vector<UHECR*> crinroi;
			for (std::vector<pxl::Serializable*>::const_iterator iter2 =
					particleinroi.begin(); iter2 != particleinroi.end(); ++iter2) {
				pxl::UHECR* cr = dynamic_cast<UHECR*> (*iter2);
				if (cr) {
					jetgraph->SetPoint(jetgraph->GetN(),
							cr->getGalacticLongitude(),
							cr->getGalacticLatitude());
					//				std::cout<<jetgraph->GetMaximum()<<std::endl;
				}
			}
		}
	}

	void sourcestograph(BasicContainer* sky, TGraph* sourcegraph) {
		std::vector<UHECRSource*> mysources;
		sky->getObjectsOfType<UHECRSource> (mysources);

		for (std::vector<pxl::UHECRSource*>::iterator iter = mysources.begin(); iter
				!= mysources.end(); ++iter) {

			pxl::UHECRSource* crsource = dynamic_cast<UHECRSource*> (*iter);

			sourcegraph->SetPoint(sourcegraph->GetN(),
					crsource->getGalacticLongitude(),
					crsource->getGalacticLatitude());
		}
	}

	inline double eec(UHECR* x1, UHECR* x2, RegionOfInterest* roi,
			TProfile* endistribution) {
		double x1angle = x1->getGalacticDirectionVector()
				* roi->getGalacticDirectionVector();
		double x2angle = x2->getGalacticDirectionVector()
				* roi->getGalacticDirectionVector();
		//CG: ROOT doesn't like zero angles
		if (x1angle >= MAXDIST) x1angle = MAXDIST;
		if (x2angle >= MAXDIST) x2angle = MAXDIST;

		double x1average = endistribution->GetBinContent(
				endistribution->FindBin(x1angle));
		double x2average = endistribution->GetBinContent(
				endistribution->FindBin(x2angle));
		double omega = (x1->getEnergy() - x1average) * (x2->getEnergy()
				- x2average) / (x1->getEnergy() * x2->getEnergy());

		logger(LOG_LEVEL_DEBUG, "x1energy", x1->getEnergy(), "x1average", x1average, "x1angle", x1angle);
		logger(LOG_LEVEL_DEBUG, "x2energy", x2->getEnergy(), "x2average", x2average, "x2angle", x2angle);
		logger(LOG_LEVEL_DEBUG, "omega", omega);
		return omega;
	}

	void calculateEEC(BasicContainer* sky, TProfile* output) {

		std::vector<RegionOfInterest*> myjets;
		sky->getObjectsOfType<RegionOfInterest> (myjets);

		double cosbins[_NBins+1];
		for (int i = 0; i < _NBins+1; i++){
			cosbins[_NBins - i] = cos(i * _SizeOfRoi / (double)_NBins);
			logger(LOG_LEVEL_DEBUG, "cosbin", _NBins-i, cosbins[_NBins-i]);
		}
		TProfile* enaverage = new TProfile("enaverage", "enaverage", _NBins,
				cosbins);

		for (std::vector<pxl::RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {

			std::vector<Serializable*> particleinroi;
			// create a pointer to the iter
			pxl::RegionOfInterest* roi = *iter;
			roi->getSoftRelations().getSoftRelatives(particleinroi, *sky);

			//remove non UHECR from vector
			std::vector<UHECR*> crinroi;

			for (std::vector<pxl::Serializable*>::const_iterator iter2 =
					particleinroi.begin(); iter2 != particleinroi.end(); ++iter2) {
				pxl::UHECR* cr = dynamic_cast<UHECR*> (*iter2);
				if (cr) {
					crinroi.push_back(cr);
					double distance = cr->getGalacticDirectionVector()
							* roi->getGalacticDirectionVector();
					//CG: ROOT doesn't like zero angles
					if (distance > MAXDIST) {
						distance = MAXDIST;
					}
					enaverage->Fill(distance, cr->getEnergy());
					logger(LOG_LEVEL_DEBUG, "enaverage.Fill(distance", distance, ",energy", cr->getEnergy(), ")");
				}
			}
			for (int i = 1; i < enaverage->GetNbinsX() + 1; i++) {
				logger(LOG_LEVEL_DEBUG, "average energy in bin", i, enaverage->GetBinContent(i));
			}
			logger(LOG_LEVEL_DEBUG, "number of particles", crinroi.size(), "=", enaverage->GetEntries());
		}
		for (std::vector<pxl::RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {

			std::vector<Serializable*> particleinroi;
			// create a pointer to the iter
			pxl::RegionOfInterest* roi = *iter;
			roi->getSoftRelations().getSoftRelatives(particleinroi, *sky);

			//remove non UHECR from vector
			std::vector<UHECR*> crinroi;

			for (std::vector<pxl::Serializable*>::const_iterator iter2 =
					particleinroi.begin(); iter2 != particleinroi.end(); ++iter2) {
				pxl::UHECR* cr = dynamic_cast<UHECR*> (*iter2);
				if (cr) {
					crinroi.push_back(cr);
				}
			}
			for (std::vector<pxl::UHECR*>::const_iterator iter2 =
					crinroi.begin(); iter2 != crinroi.end(); ++iter2) {
				for (std::vector<pxl::UHECR*>::const_iterator iter3 =
						crinroi.begin(); iter3 != crinroi.end(); ++iter3) {
					pxl::UHECR* cr1 = *iter2;
					pxl::UHECR* cr2 = *iter3;
					if (cr1 == cr2) {
						logger(LOG_LEVEL_DEBUG,"---> Not filled since cr1 == cr2");
						logger(LOG_LEVEL_DEBUG, " ");
					}
					if (cr1 != cr2) {
						double distance1 = cr1->getGalacticDirectionVector()
								* roi->getGalacticDirectionVector();
						//CG: ROOT doesn't like zero angles
						if (distance1 > MAXDIST) distance1 = MAXDIST;
						output->Fill(distance1, eec(cr1, cr2, roi, enaverage));
						logger(LOG_LEVEL_DEBUG,"---> Filled");
						logger(LOG_LEVEL_DEBUG, " ");
// 						output->Print("all");
					}
				}
			}

		}
        logger(LOG_LEVEL_DEBUG, "energy");
		for (int i = 0; i < _NBins+1; i++)
		logger(LOG_LEVEL_DEBUG, "bin", i, "entries", enaverage->GetBinEntries(i), "average", enaverage->GetBinContent(i));
		delete enaverage;
	}

	void makeEnergyDistribution(BasicContainer* sky, TProfile* enaverage) {
		std::vector<RegionOfInterest*> myjets;
		sky->getObjectsOfType<RegionOfInterest> (myjets);

		for (std::vector<pxl::RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {
			std::vector<Serializable*> particleinroi;
			// create a pointer to the iter
			pxl::RegionOfInterest* roi = *iter;
			roi->getSoftRelations().getSoftRelatives(particleinroi, *sky);

			//remove non UHECR from vector
			std::vector<UHECR*> crinroi;
			for (std::vector<pxl::Serializable*>::const_iterator iter2 =
					particleinroi.begin(); iter2 != particleinroi.end(); ++iter2) {
				pxl::UHECR* cr = dynamic_cast<UHECR*> (*iter2);
				if (cr) {
					crinroi.push_back(cr);
					//				enaverage->Fill(cr->getEnergy());

				}
			}

		}

	}

	void rebin(TProfile* input, TH1D* output) {
		logger(LOG_LEVEL_DEBUG, "EEC");
		for (int i = 1; i < input->GetNbinsX() + 1; i++) {
			output->SetBinContent(i, input->GetBinContent(_NBins+1 - i));
			output->SetBinError(i, input->GetBinError(_NBins+1 - i));
			logger(LOG_LEVEL_DEBUG, "bin", i, "entries", input->GetBinEntries(i), "average", input->GetBinContent(_NBins+1-i));
		}
	}


public:

	// initialize the super class Module as well
	EECModule() :
		Module() {
		_Count = 0;
	}

	~EECModule() {
	}

	// every Module needs a unique type
	static const std::string &getStaticType() {
		static std::string type("EEC Module");
		return type;
	}

	bool isRunnable() const {
		return false;
	}

	// static and dynamic methods are needed
	const std::string &getType() const {
		return getStaticType();
	}

	bool isExecutable() const {
		// this module does not provide events, so return false
		return false;
	}

	void initialize() {
		addSink("input", "DefaultInput");
		addSource("output", "DefaultOutput");

		addOption("SizeOfRoi", "Size of ROI", 0.2);
		addOption("FileName", "Input filename", 10.);
		addOption("OutputFolder", "Output directory", "");
	}


	void beginJob() throw (std::runtime_error) {
		getOption("SizeOfRoi", _SizeOfRoi);
		getOption("FileName", _Name);
		getOption("OutputFolder", _OutputFolder);
		ostringstream filename;
		filename << _OutputFolder << _Name << ".root";
		_Outfile = new TFile(filename.str().c_str(), "RECREATE");
		_Outfile->Print();
		//		_Outfile->Close();
	}

	void endJob() {

		_Outfile->Close();

	}


	bool analyse(pxl::Sink* sink) throw (std::runtime_error) {


		//		ostringstream filename;
		//		filename << _OutputFolder << _Name << ".root";
		//		_Outfile = new TFile(filename.str().c_str(), "UPDATE");

		std::cout << "EEC: Analyse" << std::endl;

		BasicContainer *container = dynamic_cast<BasicContainer *> (getSink(
				"input")->get());

		if (container) {
			//Define the histograms and graphs

			ostringstream thisname;
			thisname << _Name;
			cout << thisname.str() << endl;

			ostringstream h_omega_name, h_omega2_name, h_sky_name,
					h_sources_name, h_jets_name, h_angres_name, g_sky_name,
					g_sources_name, g_jets_name;
			h_omega_name << thisname.str() << ".h_omega." << _Count;
			h_omega2_name << thisname.str() << ".h_omega2." << _Count;
			h_sky_name << thisname.str() << ".h_sky." << _Count;
			h_sources_name << thisname.str() << ".h_sources." << _Count;
			h_jets_name << thisname.str() << ".h_jets." << _Count;
			h_angres_name << thisname.str() << ".h_angres." << _Count;
			g_sky_name << thisname.str() << ".g_sky." << _Count;
			g_sources_name << thisname.str() << ".g_sources." << _Count;
			g_jets_name << thisname.str() << ".g_jets." << _Count;

			double cosbins[_NBins+1];
			for (int i = 0; i < _NBins+1; i++)
				cosbins[_NBins - i] = cos(i * _SizeOfRoi / (double)_NBins);

			TProfile* h_omega = new TProfile(h_omega_name.str().c_str(),
					"myomega", _NBins, cosbins);
			TH1D* h_omega2 = new TH1D(h_omega2_name.str().c_str(), "myomega2",
					_NBins, 0, _SizeOfRoi);
			TH2D* h_sky = new TH2D(h_sky_name.str().c_str(), "sky", 200, -M_PI,
					M_PI, 100, -M_PI / 2, M_PI / 2);
			TH2D* h_sources = new TH2D(h_sources_name.str().c_str(), "sources",
					200, -M_PI, M_PI, 100, -M_PI / 2, M_PI / 2);
			TH2D* h_jets = new TH2D(h_jets_name.str().c_str(), "jets", 200,
					-M_PI, M_PI, 100, -M_PI / 2, M_PI / 2);
			TH1D* h_angres = new TH1D(h_angres_name.str().c_str(), "angres",
					20, 0, 0.6);
			TGraph* g_sky = new TGraph();
			g_sky->SetNameTitle(g_sky_name.str().c_str(), "sky");
			TGraph* g_sources = new TGraph();
			g_sources->SetNameTitle(g_sources_name.str().c_str(), "sources");
			TGraph* g_jets = new TGraph();
			g_jets->SetNameTitle(g_jets_name.str().c_str(), "jets");

			jetstohist(container, h_jets);
			jetstograph(container, g_jets);

			sourcestograph(container, g_sources);

			uhecrstohist(container, h_sky);
			uhecrstograph(container, g_sky);

			calculateEEC(container, h_omega);

			rebin(h_omega, h_omega2);

			h_omega->Write();
			h_omega2->Write();
			h_sky->Write();
			h_sources->Write();
			h_jets->Write();
			h_angres->Write();
			g_sky->Write();
			g_sources->Write();
			g_jets->Write();
			_Outfile->Write();
			delete h_omega;
			delete h_omega2;
			delete h_sky;
			delete h_sources;
			delete h_jets;
			delete h_angres;
			delete g_sky;
			delete g_sources;
			delete g_jets;
		} //if BasicContainer
		
		else {
			std::cerr << "Something other than a BasicContainer passed by\n";
		}
		_Count++;

		logger(LOG_LEVEL_DEBUG, "still running");
		_Source.setTargets(_Sink.get());
		return _Source.processTargets();

		// simply pass the events from input to output
		//		_Outfile->Close();
	}

	void destroy() throw (std::runtime_error) {
		// only we know how to delete this module
		delete this;
	}

};
PXL_MODULE_INIT(EECModule)
PXL_PLUGIN_INIT
